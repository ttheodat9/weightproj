var file = '';
var img = new Image(); //Creates a new image

window.onload = function () {
    $('#selectImage').change(function (e) { //Event that is triggred when the file changes
        file = e.target.files[0]; //Variable that targets the file selected by the element selector (fileInput) 
        var imageType = /image.*/; //A variable to filter file type

        if (file.type.match(imageType)) { //It creates the instance of FileReader API only if the file type matches
            var reader = new FileReader(); //Creates the reader
            reader.readAsDataURL(file); //Reads the content of the file
            reader.onload = function (e) { //When the file finished loading, we can access the result
                $('#picture').html(''); //Clears the DIV where the image will be displayed
                img.src = reader.result; //Set the img src property using the data URL
                // userWeight.picture = img.src;
                $('#picture').append(img); //Add the image to the DIV
            };
        } else {
            $('#picture').html('File not supported!'); //If the file selected is not supported, a message is displayed
        }
    });
};
var userWeight = {
    firstname: '',
    lastname: '',
    phone: '',
    email: '',
    picture: '',
    weight: []
}
weight = {
    pounds: '',
    date: ''
}
var checkIt = false;

$('#submitWeight').click(() => {
    userWeight.firstname = $('#firstname').val();
    userWeight.lastname = $('#lastname').val();
    userWeight.phone = $('#phone').val();
    userWeight.email = $('#email').val();
    weight.pounds = $('#weight').val();
    userWeight.picture = img.src;
    weight.date = $('#date').val();
    userWeight.weight.push(weight);

    if (userWeight.firstname == '' || userWeight.lastname == '' || userWeight.phone == '' || isNaN(userWeight.phone) || userWeight.email == '' || weight.pounds == '' || weight.date == '') {
        swal('Weight Registration Declined', 'Please fill out the entire form correctly', 'error');
        return;
    }
    else if (file == null) {
        swal('Image?', 'Please select an image', 'warning');
        return;
    }
    else {
        var clients = JSON.parse(localStorage.getItem('Clients'));

        if (clients == null)
            clients = [];
        for (let index = 0; index < clients.length; index++) {
            if (userWeight.phone == clients[index].phone) {
                swal('Update', `Currently updating the information for ${userWeight.phone}`, 'info');
                clients[index].firstname = userWeight.firstname;
                clients[index].lastname = userWeight.lastname;
                clients[index].email = userWeight.email;
                clients[index].picture = userWeight.picture;

                checkIt = true;
                var dateMatch = false;

                for (let secondIndex = 0; secondIndex < clients[index].weight.length; secondIndex++) {
                    if (weight.date == clients[index].weight[secondIndex].date) {
                        dateMatch = true;
                        clients[index].weight[secondIndex].pounds = weight.pounds;
                        continue;
                    }
                }
                if (!dateMatch) {
                    clients[index].weight.push(weight);
                }
            }
        }
        if (!checkIt) {
            clients.push(userWeight);
            swal('Weight Registration Accepted', `${userWeight.firstname} ${userWeight.lastname}'s info has been saved.`, 'success');

        }
        localStorage.setItem('Clients', JSON.stringify(clients));
    }
    $('#firstname').val('');
    $('#lastname').val('');
    $('#picture').html('');
    $('#selectImage').val('');
    $('#phone').val('');
    $('#email').val('');
    $('#weight').val('');
    $('#date').val('');
    $('#firstname').focus();
});

$(document).ready(() => {
    var clients = JSON.parse(localStorage.getItem('Clients'));
    $('#undoHidden').prop('hidden', false);
    if (clients == null || clients.length <= 0) {
    }
    else {
        displayData();
    }
});

function displayData() {
    var clients = JSON.parse(localStorage.getItem('Clients'));
    var displayInfo = '';
    $('#undoHidden').prop('hidden', false);

    if (clients == null || clients.length <= 0) {
        $('#displayWeight').html('');

    } else {
        var displayInfo = `<thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Profile Picture</th>
            <th>Weight</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>`;
        for (let index = 0; index < clients.length; index++) {
            displayInfo += `<tr>
            <td> ${clients[index].firstname} </td>
            <td> ${clients[index].lastname} </td>
            <td> ${clients[index].phone} </td>
            <td> ${clients[index].email} </td>
            <td><img src="${clients[index].picture}" width="80" height="80" style="border-radius: 15px; border: groove"></td>
            <td><i style="text-decoration: underline" onclick=\'showDetails(${JSON.stringify(clients[index])})\'>Details with Modal</i></td>
            <td><i class="fa fa-trash-o" title="Delete info for this date" onclick=\"dateRemoval(${index})\"></i></td>
            </tr>`;
        }
        $('#displayWeight').html(displayInfo + '</tbody>');
    }
}

function showDetails(client) {
    var displayWeight = '';
    for (let index = 0; index < client.weight.length; index++) {
        displayWeight += `${client.weight[index].date} - ${client.weight[index].pounds} lbs<br\>`;
    }
    $('#weightData').html(displayWeight);
    $('#showModal').modal('show');

}

function dateRemoval(index) {
    var clients = JSON.parse(localStorage.getItem('Clients'));
    swal({
        title: `Confirm Removal of the Data`,
        text: 'Are you sure you want to delete this user\'s info?',
        buttons: {
            cancel: "Cancel",
            catch: {
                text: "Confirm",
                value: "delete",
            },
        },
        icon: 'warning'
    }).then((value) => {
        switch (value) {
            case "delete":
                clients.splice(index, 1);
                localStorage.setItem('Clients', JSON.stringify(clients));
                displayData();
                break;
            default:
                displayData();
        }
    });
}


$('#sendMessage').click(() => {
    var messageStatus = {
        Name: '',
        Email: '',
        Subject: '',
        Message: ''
    }
    messageStatus.Name = $('#name').val();
    messageStatus.Email = $('#EMAIL').val();
    messageStatus.Subject = $('#subject').val();
    messageStatus.Message = $('#message').val();
    var messageSent = JSON.parse(localStorage.getItem('Message Receipts'));

    if (messageStatus.Name == '' || messageStatus.Email == '' || messageStatus.Message == '') {
        swal('Cannot Send Message', 'At least fill out all inputs with an asterisk (*)', 'error');
        return;
    }

    else if (messageStatus.Subject == '') {
        swal({
            title: `No Subject Name`,
            text: 'Would you like to proceed without giving your message a subject?',
            buttons: {
                cancel: "No",
                catch: {
                    text: "Yes",
                    value: "proceed",
                },
            },
            icon: 'info'
        }).then((value) => {
            switch (value) {
                default:
                    $('#subject').focus();
                    break;
                case "proceed":
                    messageSent.push(messageStatus);
                    localStorage.setItem('Message Receipts', JSON.stringify(messageSent));

            }
        });
        return;
    }

    if (messageSent == null)
        var messageSent = [];
    messageSent.push(messageStatus);
    localStorage.setItem('Message Receipts', JSON.stringify(messageSent));
    $('#name').val('');
    $('#EMAIL').val('');
    $('#subject').val('');
    $('#message').val('');
    $('#name').focus();
});

//Mailing List (footer)
$('.subscribe').click(() => {
    var list = JSON.parse(localStorage.getItem('Mailing List'));
    var email = $('.mail').val();
    if (email == '') {
        swal('EMAIL NEEDED!!!', 'enter your email', 'warning');
        return;
    }
    else {
        if (list == null)
            var list = [];
        list.push(email);
        localStorage.setItem('Mailing List', JSON.stringify(list));
        $('.mail').val('');
    }
});